$( document ).ready(function() {
    var statuslogin=localStorage.getItem('opnicare-statuslogin');


    if(statuslogin!=='on'){
	    document.getElementById('btpopuplogin').click();
    }

    setTimeout(function(){ 
		document.getElementById('splash').style='display:none';
		document.getElementById('app').style='display:block';
    }, 2000);			            
    backToList();
   // document.addEventListener("deviceready", onDeviceReady, false);


});

function showLogin(){
	document.getElementById('my-signup-screen').style='display:none';
	document.getElementById('btpopuplogin').click();
}

function showSignup(){
	document.getElementById('my-signup-screen').style='display:block';
	document.getElementById('btpopupsignup').click();
}

function signup(){
	var nama =document.getElementById('nama').value;
	var email =document.getElementById('email').value;
	var pass =document.getElementById('pass').value;
	var phone =document.getElementById('phone').value;
	var gender =document.getElementById('gender').value;
	var birth =document.getElementById('birth').value;

    var postData=new Object();
        postData['userName']=nama;
        postData['userEmail']=email;
        postData['userPassword']=pass;
        postData['userPhone']=phone;
        postData['userGender']=gender;
        postData['userBirth']=birth;


	if(nama==='') {
		var dialog = app.dialog.alert('Isian nama masih kosong !!!','Peringatan');
		dialog.open();
		return;
	}

	if(email==='') {
		var dialog = app.dialog.alert('Isian e-mail masih kosong !!!','Peringatan');
		dialog.open();
		return;
	}

	if(pass==='') {``
		var dialog = app.dialog.alert('Isian password masih kosong !!!','Peringatan');
		dialog.open();
		return;
	}

    $.ajax({
        type: "GET",
        url: "https://optimasolution.co.id/apiservice/public/api/admin/mobileuser",
        data:({'userEmail':email}),
        dataType:"json",
        success:function(data){
        	if(data.length>0){
				var dialog = app.dialog.alert('Email yang anda gunakan sudah terdaftar !','Peringatan');
				dialog.open();
				return;
        	}else{
			    $.ajax({
			        type: "POST",
			        url: "https://optimasolution.co.id/apiservice/public/api/admin/mobileuser",
			        data:postData,
			        dataType:"json",
			        success:function(data){

						var dialog = app.dialog.alert('Registrasi Anda Berhasil !','Informasi',function(){
				            showLogin();        
				            setTimeout(function(){ 
				            	document.getElementById('username').value=email;
				            	document.getElementById('password').value='';
				            }, 200);			            
						});
						dialog.open();

			        }
			    });      
        	}
        }
    });      
}


function update(){
	var nama =document.getElementById('snama').value;
	var email =document.getElementById('semail').value;
	var pass =document.getElementById('spass').value;
	var phone =document.getElementById('sphone').value;
	var gender =document.getElementById('sgender').value;
	var birth =document.getElementById('sbirth').value;
	var id =document.getElementById('sid').value;

    var postData=new Object();
        postData['userName']=nama;
        postData['userEmail']=email;
        if(pass!=''){
        	postData['userPassword']=pass;
        }
        postData['userPhone']=phone;
        postData['userGender']=gender;
        postData['userBirth']=birth;



	if(nama==='') {
		var dialog = app.dialog.alert('Isian nama masih kosong !!!','Peringatan');
		dialog.open();
		return;
	}

	if(email==='') {
		var dialog = app.dialog.alert('Isian e-mail masih kosong !!!','Peringatan');
		dialog.open();
		return;
	}


    $.ajax({
        type: "PUT",
        url: "https://optimasolution.co.id/apiservice/public/api/admin/mobileuser/"+id,
        data:postData,
        dataType:"json",
        success:function(data){
        	// console.log(data);
        	if(data.status.error===0){
				var dialog = app.dialog.alert('Data berhasil disimpan!','Informasi');
				dialog.open();

        		localStorage.setItem('opnicare-nama', nama);
        		localStorage.setItem('opnicare-birth', birth);
        		localStorage.setItem('opnicare-email', email);
        		localStorage.setItem('opnicare-gender', gender);
        		localStorage.setItem('opnicare-phone', phone);

        	}else{
				var dialog = app.dialog.alert('Terjadi kesalahan!','Peringatan');
				dialog.open();
        	}
        }
    });      
}





function login(){
    var email=document.getElementById('username').value;
    var pass=document.getElementById('password').value;

    $.ajax({
        type: "GET",
        url: "https://optimasolution.co.id/apiservice/public/api/admin/mobilelogin",
        data:({'userEmail':email,'userPassword':pass}),
        dataType:"json",
        success:function(data){
        	if(data.length>0){
				document.getElementById('closeleftpanel').click();
        		localStorage.setItem('opnicare-statuslogin', 'on');
        		localStorage.setItem('opnicare-id', data[0].userId);
        		localStorage.setItem('opnicare-nama', data[0].userName);
        		localStorage.setItem('opnicare-birth', data[0].userBirth);
        		localStorage.setItem('opnicare-email', data[0].userEmail);
        		localStorage.setItem('opnicare-gender', data[0].userGender);
        		localStorage.setItem('opnicare-phone', data[0].userPhone);
        		localStorage.setItem('opnicare-info', []);
			    document.getElementById('btloginclose').click();

        	}else{
				var dialog = app.dialog.alert('User belum terdaftar / Password salah !!!','Peringatan');
				dialog.open();
				return;
        	}
        }
    });      

}

function logout(){
	localStorage.setItem('opnicare-statuslogin', 'off');

	localStorage.setItem('opnicare-id', '');
	localStorage.setItem('opnicare-nama', '');
	localStorage.setItem('opnicare-birth', '');
	localStorage.setItem('opnicare-email', '');
	localStorage.setItem('opnicare-gender', '');
	localStorage.setItem('opnicare-phone', '');
	localStorage.setItem('opnicare-info', []);

    showLogin();        
}

function settingsClick(){
	document.getElementById('sid').value=localStorage.getItem('opnicare-id');
	document.getElementById('snama').value=localStorage.getItem('opnicare-nama');
	document.getElementById('semail').value=localStorage.getItem('opnicare-email');
	document.getElementById('sphone').value=localStorage.getItem('opnicare-phone');
	document.getElementById('sgender').value=localStorage.getItem('opnicare-gender');
	document.getElementById('sbirth').value=localStorage.getItem('opnicare-birth');
	document.getElementById('spass').value='';

}


function createMap(latx,lngx){
    var userpos = { lat: latx, lng: lngx};
	var gmap= {
		center:new google.maps.LatLng(userpos),
		zoom:15.5,
	};
	var map = new google.maps.Map(document.getElementById("map"),gmap);

	var marker = new google.maps.Marker({
		position: userpos,
		animation:google.maps.Animation.BOUNCE,
		map:map
	});



    $.ajax({
        type: "GET",
        url: "https://optimasolution.co.id/apiservice/public/api/admin/getdokterarea",
        data:({'lat':latx,'lng':lngx}),
        dataType:"json",
        success:function(data){

        	var marker2=[];
        	var infowindow = [];
        	for(var i=0;i<=data.length-1;i++){
	        	var xlat=Number(data[i].dokLat);
	        	var xlng=Number(data[i].dokLng);
	        	var nama=data[i].dokNama;
	        	var spesialis=data[i].dokSpesialisasi;
			    var pos = { lat: xlat, lng: xlng};


				var a  ="var marker2"+i+" =new google.maps.Marker({";
					a +="position: pos,";
					a +="map:map,";					
					a +="icon:{url:'img/mapicon.png'},";					
					a +="});";
					eval(a);



				var content  ='<div><b>'+nama+'</b></div>';
				    content +='<div style="font-size: 11px;margin-bottom:8px;">'+spesialis+'</div>';
				    content +='<a href="#" onclick="formDaftar('+data[i].dokId+')" class="col button button-small button-round button-fill" style="color: white">Detail</a>';

				var b  ="var infowindow"+i+" =new google.maps.InfoWindow({";
					b +="content:'"+content+"'";
					b +="});";
					eval(b);

		        var c  ="marker2"+i+".addListener('click', function() {";
		        	c +="  infowindow"+i+".open(map, marker2"+i+");"
		        	c +="});";
		        	eval(c);

				var d="infowindow"+i+".open(map,marker2"+i+");";
					eval(d);

				map.setCenter();
			}
        }
    });      



}


function onDeviceReady() {
    navigator.geolocation.getCurrentPosition(onSuccess, onError);
}

function onSuccess(position) {
    createMap(position.coords.latitude, position.coords.longitude);
}

function onError(error) {
    console.log('code: ' + error.code + '\n' +
        'message: ' + error.message + '\n');
}


function formDaftar(id){

    $.ajax({
        type: "GET",
        url: "https://optimasolution.co.id/apiservice/public/api/admin/getdokterarea/"+id,
        dataType:"json",
        success:function(datax){
        	var data=datax.data;

		    $.ajax({
		        type: "GET",
		        url: "https://optimasolution.co.id/apiservice/public/api/admin/mobileklinik/"+data.dokKlinik,
		        dataType:"json",
		        success:function(datay){
					var data2=datay.data;

		        	document.getElementById('dokId').value=data.dokId;
		        	document.getElementById('dokNama').value=data.dokNama;
		        	document.getElementById('dokSpesialisasi').value=data.dokSpesialisasi;
		        	document.getElementById('dokKlinik').value=data2.klinikNama;
		        	document.getElementById('dokKlinikALamat').value=data2.klinikAlamat;
		        	document.getElementById('dokApi').value=data2.klinikHostApi;

		        	document.getElementById('pasienPenanggungJawab').value=localStorage.getItem('opnicare-nama');
		        	document.getElementById('pasienPenanggungJawabTelp').value=localStorage.getItem('opnicare-phone');
		        	
					var today = new Date();
					var dd = String(today.getDate()).padStart(2, '0');
					var mm = String(today.getMonth() + 1).padStart(2, '0');
					var yyyy = today.getFullYear();

		        	document.getElementById('pasienTanggalBerobat').value=yyyy+'-'+mm+'-'+dd;



					var userId=localStorage.getItem('opnicare-id');

				    $.ajax({
				        type: "GET",
				        url: data2.klinikHostApi+"getlistpasien",
				        data: ({'mobileId':userId}),
				        dataType:"json",
				        success:function(datac){
				        	var res  ='<select id="pasienNama2">';
				        		// res +='<option value="" selected></option>';				        		

				        	for(var i=0;i<=datac.length-1;i++){
				        		res +='<option value="'+datac[i].msPasRm+'">'+datac[i].msPasNama+'</option>';				        		
				        	}
				        		res +='</select>';

				        	$('#namaLama').html(res);

							document.getElementById('jenispasien').value=1;
							gantiPasien(1);
							document.getElementById('btDaftar').click();
				        }
				    });      

		        }
		    });      


        }
    });      


}


function gantiPasien(val){
	if(val==0){
    	$('#liPasienLama').hide();
    	$('#liPasienBaru').show();
	}else if(val==1){
    	$('#liPasienLama').show();
    	$('#liPasienBaru').hide();
	}
}


function prosesDaftar(){

	var btstatus=localStorage.getItem('opnicare-ondaftar') || 'off';
	if(btstatus=='on') return;

	var dokApi=document.getElementById('dokApi').value;
	var userId=localStorage.getItem('opnicare-id');
	var dokId=document.getElementById('dokId').value;
	var dokNama=document.getElementById('dokNama').value;

	var tgdaftar=document.getElementById('pasienTanggalBerobat').value;
	var namapasien=document.getElementById('pasienNama').value;
	var nomorRm=document.getElementById('pasienNama2').value;
	var lahirpasien=document.getElementById('pasienLahir').value;
	var nobpjs=document.getElementById('pasienBpjs').value;
	var tgjawab=document.getElementById('pasienPenanggungJawab').value;
	var tgjawabtlp=document.getElementById('pasienPenanggungJawabTelp').value;
	var identitas=document.getElementById('pasienIdentitas').value;
	var pasienGender=document.getElementById('pasienGender').value;

	var jenispasien=document.getElementById('jenispasien').value;

	if(namapasien=='' && jenispasien==0){
		var dialog = app.dialog.alert('Nama pasien masih kosong !!!','Peringatan');
		dialog.open();
		return;
	}

	if(nomorRm=='' && jenispasien==1){
		var dialog = app.dialog.alert('Nama pasien masih kosong !!!','Peringatan');
		dialog.open();
		return;
	}

	if(jenispasien==0){
		nomorRm='';
	}

	localStorage.setItem('opnicare-ondaftar', 'on');

    $.ajax({
        type: "GET",
        url: dokApi+"cekmasterpasien",
        dataType:"json",
        data:({mobileId:userId,norm:nomorRm}),
        success:function(data){

        	if(data.length>0 && jenispasien==1){
        		//insert trRawat
			    var postData=new Object();
			        postData['rawatDokterId']=dokId;
			        postData['rawatTglDaftar']=tgdaftar;
			        postData['rawatNama']=data[0].msPasNama;
			        postData['rawatLahir']=data[0].msPasLahir;
			        postData['rawatNoKartu']=nobpjs;
			        postData['rawatTgJawab']=tgjawab;
			        postData['rawatTgTlp']=tgjawabtlp;
			        postData['rawatJenis']=1;
			        postData['rawatMobileId']=userId;
			        postData['rawatKtp']=identitas;
			        postData['rawatRm']=data[0].msPasRm;
			        postData['rawatGender']=data[0].msPasGender;

			    $.ajax({
			        type: "POST",
			        url: dokApi+"daftar",
			        dataType:"json",
			        data:postData,
			        success:function(data2){

						var now     = new Date(); 
				        var year    = now.getFullYear();
				        var month   = now.getMonth()+1; 
				        var day     = now.getDate();
				        var hour    = now.getHours();
				        var minute  = now.getMinutes();
				        var second  = now.getSeconds(); 
				        var urut    = year+'-'+month+'-'+day+' '+hour+':'+minute+':'+second;

						var konten='Pasien atas nama '+data[0].msPasNama+' telah didaftarkan berobat ke '+dokNama+' pada tanggal '+tgdaftar+' dengan nomor antrian '+data2.data.rawatUrutDaftar;
						
						if(localStorage.getItem('opnicare-info')==''){
							var info = [];
						}else{
							var info = JSON.parse(localStorage.getItem('opnicare-info'));
						}
						
						var dataInfo = {
										 id:urut,
										 jenis:1,
										 title:dokNama,
										 description : konten,
										 read : false,
										 norm : data2.data.rawatRm,
										 tgdaftar : tgdaftar,
										 idDokter : dokId
									   };
						info.push(dataInfo);
						localStorage.setItem('opnicare-info',JSON.stringify(info));
						reloadData();

			        	document.getElementById('pasienNama').value='';
			        	document.getElementById('pasienIdentitas').value='';
			        	document.getElementById('pasienBpjs').value='';
						document.getElementById('bthome').click();

						var dialog = app.dialog.alert('Pendaftaran Berhasil !!! Anda Mendapat Nomor Antrian ke '+data2.data.rawatUrutDaftar,'Informasi');
						dialog.open();
						localStorage.setItem('opnicare-ondaftar', 'off');
			        }
			    });      

        	}else if(data.length==0 && jenispasien==0){
        		//insert msPasien

			    var postData=new Object();
			        postData['msPasDokterId']=dokId;
			        postData['msPasTglDaftar']=tgdaftar;
			        postData['msPasNama']=namapasien;
			        postData['msPasLahir']=lahirpasien;
			        postData['msPasNoKartu']=nobpjs;
			        postData['msPasTgJawab']=tgjawab;
			        postData['msPasTgTlp']=tgjawabtlp;
			        postData['msPasJenis']=1;
			        postData['msPasMobileId']=userId;
			        postData['msPasKtp']=identitas;
			        postData['msPasGender']=pasienGender;

			    $.ajax({
			        type: "POST",
			        url: dokApi+"masterpasien",
			        dataType:"json",
			        data:postData,
			        success:function(data2){

						var now     = new Date(); 
				        var year    = now.getFullYear();
				        var month   = now.getMonth()+1; 
				        var day     = now.getDate();
				        var hour    = now.getHours();
				        var minute  = now.getMinutes();
				        var second  = now.getSeconds(); 
				        var urut    = year+'-'+month+'-'+day+' '+hour+':'+minute+':'+second;

						var konten='Pasien atas nama '+data2.data.msPasNama+' telah didaftarkan berobat ke '+dokNama+' pada tanggal '+tgdaftar+' dengan nomor antrian '+data2.data.msPasUrutDaftar;
						if(localStorage.getItem('opnicare-info')==''){
							var info = [];
						}else{
							var info = JSON.parse(localStorage.getItem('opnicare-info'));
						}
						var dataInfo = {
										 id:urut,
										 jenis:1,
										 title:dokNama,
										 description : konten,
										 read : false,
										 norm : data2.data.msPasRm,
										 tgdaftar : tgdaftar,
										 idDokter : dokId
									   };
						info.push(dataInfo);
						localStorage.setItem('opnicare-info',JSON.stringify(info));

						reloadData();

			        	document.getElementById('pasienNama').value='';
			        	document.getElementById('pasienIdentitas').value='';
			        	document.getElementById('pasienBpjs').value='';
						document.getElementById('bthome').click();
						var dialog = app.dialog.alert('Pendaftaran Berhasil !!! Anda Mendapat Nomor Antrian ke '+data2.data.msPasUrutDaftar,'Informasi');
						dialog.open();
						localStorage.setItem('opnicare-ondaftar', 'off');
			        }
			    });      

        	}
        }
    });     

}

function reloadData(){
	try{
		if(JSON.parse(localStorage.getItem('opnicare-info')).length==0) return;

		var dataarray=JSON.parse(localStorage.getItem('opnicare-info'));
		var result=[];
		for(var i=dataarray.length-1;i>=0;i--){
			result.push(dataarray[i]);
		}

		app.data.products=result;
		catalogView.router.navigate('/catalog/', {
		  reloadCurrent: true,
		  ignoreCache: true,
		});

		var data = app.data.products;
		var unread=0;
		for(var i=0;i<=data.length-1;i++){
			if(data[i].read==false){
				unread +=1;
			}
		}

		if(unread==0){
			$('#badge1').html('');
			$('#badge2').html('');
			$('#badge3').html('');
		}else{
			$('#badge1').html('<span class="badge color-red">'+unread+'</span>');
			$('#badge2').html('<span class="badge color-red">'+unread+'</span>');
			$('#badge3').html('<span class="badge color-red">'+unread+'</span>');
		}

	}catch(e){}
}

function updateRead(id){
	var data = app.data.products;
	for(var i=0;i<=data.length-1;i++){
		if(data[i].id==id){
			data[i].read=true;			
		}
	}

	var result=[];
	for(var i=data.length-1;i>=0;i--){
		result.push(data[i]);
	}

	localStorage.setItem('opnicare-info',JSON.stringify(result));
	
}

function backToList(){
	setTimeout(function(){ 
		reloadData();
	}, 500);
}

function batal(){
	app.dialog.confirm('Anda yakin akan membatalkan ???', 'Konfirmasi', ifyes,ifno);
}
function ifno(){

}

function ifyes(){
	var dokApi=document.getElementById('dokApi').value;
	var id=document.getElementById('idproduck').value;
	var norm=document.getElementById('norm').value;
	var tgdaftar=document.getElementById('tgdaftar').value;
	var idDokter=document.getElementById('idDokter').value;

    $.ajax({
        type: "GET",
        url: dokApi+"rawatfordelete",
        dataType:"json",
        data:({norm:norm,tgdaftar:tgdaftar,norm:norm,idDokter:idDokter}),
        success:function(data){
        	if(data.length>0){
	        	var idrawat=data[0].rawatId;
			    $.ajax({
			        type: "DELETE",
			        url: dokApi+"rawatfordelete/"+idrawat,
			        dataType:"json",
			        success:function(data2){

						var dataarray=JSON.parse(localStorage.getItem('opnicare-info'));
						var result=[];
						for(var i=0;i<=dataarray.length-1;i++){
							if(dataarray[i].id != id){
								result.push(dataarray[i]);
							}
						}
						localStorage.setItem('opnicare-info',JSON.stringify(result));
						document.getElementById('btback').click();
						backToList();

			        }
			    });      
        	}else{
						var dataarray=JSON.parse(localStorage.getItem('opnicare-info'));
						var result=[];
						for(var i=0;i<=dataarray.length-1;i++){
							if(dataarray[i].id != id){
								result.push(dataarray[i]);
							}
						}
						localStorage.setItem('opnicare-info',JSON.stringify(result));
						document.getElementById('btback').click();
						backToList();
        	}
        }
    });      

}